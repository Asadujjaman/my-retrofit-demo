package com.example.retrofitprojectdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class MainActivity extends AppCompatActivity {
    private  JsonPlaceHolderApi jsonPlaceHolderApi;
    private TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewResult = findViewById(R.id.text_view_result);

        Gson gson = new GsonBuilder().serializeNulls().create();

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @NotNull
                    @Override
                    public okhttp3.Response intercept(@NotNull Chain chain) throws IOException {
                        Request originalRequest=chain.request();

                        Request newRequest = originalRequest.newBuilder()
                                .header("Interceptor-Header", "xyz")
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .addInterceptor(loggingInterceptor)   //okhttp ude here to show in logcat terminal,its connected to server to phone request system.
                .build();


        Retrofit retrofit = new Retrofit.Builder()         //this one for json place holder to make retrofit object.
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        /*Call<List<Post>> call =jsonPlaceHolderApi.getPosts();
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {

                if (!response.isSuccessful()){
                    textViewResult.setText("Code "+response.code());
                    return;
                }
                List<Post> posts = response.body();
                for (Post post:posts){     //for each loop using for each post in our post list.
                    String content ="";
                    content += "ID: "+ post.getId() + "\n";
                    content += "User ID: "+ post.getUserID() + "\n";
                    content += "Title: "+ post.getTitle() + "\n";
                    content += "Text:" + post.getText() + "\n\n";

                    textViewResult.append(content);  //this is for show in phone screen all of the data from json format in a gson response.
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });*/
        getPosts();
       // getCommnets();
      //  createPost();
      //  updatePost();
       // deletePost();
    }
    private void getPosts(){
        Map<String,String>parameters = new HashMap<>();
        parameters.put("userId","1");
        parameters.put("_sort","id");
        parameters.put("_order","desc");
        Call<List<Post>> call =jsonPlaceHolderApi.getPosts(parameters);
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {

                if (!response.isSuccessful()){
                    textViewResult.setText("Code "+response.code());
                    return;
                }
                List<Post> posts = response.body();
                for (Post post:posts){     //for each loop using for each post in our post list.
                    String content ="";
                    content += "ID: "+ post.getId() + "\n";
                    content += "User ID: "+ post.getUserID() + "\n";
                    content += "Title: "+ post.getTitle() + "\n";
                    content += "Text:" + post.getText() + "\n\n";

                    textViewResult.append(content);  //this is for show in phone screen all of the data from json format in a gson response.
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });
    }

    private void getCommnets(){
        Call<List<Comment>> call = jsonPlaceHolderApi.getComments("posts/3/comments");

        call.enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {

                if (!response.isSuccessful()){
                    textViewResult.setText("Code "+response.code());
                    return;
                }
                List<Comment> comments = response.body();
                for (Comment comment : comments){
                    String content ="";
                    content += "ID: "+ comment.getId()+ "\n";
                    content += "Post ID: "+comment.getPostID()+"\n";
                    content += "Name: "+comment.getName()+"\n";
                    content += "Email: "+comment.getEmail()+"\n";
                    content += "Text: "+comment.getText()+"\n";

                    textViewResult.append(content);
                }
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });
    }
    private void createPost(){
        final Post post = new Post(23,"New Title","New Text");

        Map<String,String> fields = new HashMap<>();
        fields.put("userId","25");
        fields.put("title","New Title");

        Call<Post> call = jsonPlaceHolderApi.createPost(fields);  //now its (23,"New Title","New Text") removed
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if (!response.isSuccessful()){
                    textViewResult.setText("Code: "+response.code());
                    return;
                }
                Post postResponse = response.body();

                String content = "";
                content += "Code: "+response.code()+"\n";
                content += "ID: "+postResponse.getId()+"\n";
                content += "User ID: "+postResponse.getUserID()+"\n";
                content += "Title: "+postResponse.getTitle()+"\n";
                content += "Text: "+postResponse.getText()+"\n\n";

                textViewResult.setText(content);
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });

    }

    private void updatePost(){
        Post post = new Post(12,null,"New Text");

        Map<String, String> headers = new HashMap<>();  //it is for show header in logcat
        headers.put("Map-Header1", "def");
        headers.put("Map-Header2", "ghi");
        Call<Post> call = jsonPlaceHolderApi.patchPost(headers,5,post);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                if (!response.isSuccessful()){
                    textViewResult.setText("Code: "+response.code());
                    return;
                }
                Post postResponse = response.body();

                String content = "";
                content += "Code: "+response.code()+"\n";
                content += "ID: "+postResponse.getId()+"\n";
                content += "User ID: "+postResponse.getUserID()+"\n";
                content += "Title: "+postResponse.getTitle()+"\n";
                content += "Text: "+postResponse.getText()+"\n\n";

                textViewResult.setText(content);



            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });
    }
    private void deletePost(){
        Call<Post> call = jsonPlaceHolderApi.deletePost(5);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                textViewResult.setText("Code: "+response.code());
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });
    }

}
